from selenium import webdriver

options = webdriver.ChromeOptions()
mode = input("Headless mode? y/n \n")
if mode == "y":
    options.headless = True
    options.add_argument('window-size=1920x1480')
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    browser = webdriver.Chrome(options=options)
else:
    browser = webdriver.Chrome()

browser.get("https://nycitybride.com")
