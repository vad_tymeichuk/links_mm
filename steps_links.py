from start_point import browser
import funcs


funcs.feb_cn('mega-menu-link')
funcs.feb_lt('All wedding dresses')


# Verifies "Wedding dresses" page (Home -> Wedding Dresses -> All wedding dresses)
def test_verif_1(teardown):
    try:
        funcs.verif_h1("h1", "Wedding Dresses")
    except:
        assert False


print("'Wedding dress' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("New Arrivals")


# Verifies "New Arrivals" page (Home -> All wedding dresses -> New Arrivals)
def test_verif_2(teardown):
    try:
        funcs.verif_h1("h1", "New Arrivals")
    except:
        assert False


print("'New Arrivals' page confirmed!")


funcs.feb_cn('mega-menu-link')
funcs.feb_lt('Plus Size Wedding Dresses')

# Verifies "Plus Size Wedding Dresses" page (Home -> All wedding dresses -> Plus Size Wedding Dresses)
def test_verif_3(teardown):
    try:
        funcs.verif_plus_size("woof_remove_ppi", "h1")
    except:
        assert False


print("'Plus Size Wedding Dresses' page confirmed!")


funcs.feb_cn('mega-menu-link')
funcs.feb_lt('Dresses under $400')


# Verifies "Dresses under $400" page (Home -> All wedding dresses -> Dresses under $400)
def test_verif_4(teardown):
    try:
        funcs.verif_under_400('min_price', 'max_price')
    except:
        assert False


print("'Dresses under $400' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("Sale")


# Verifies "Wedding Dresses on Sale" page (Home -> All wedding dresses -> Sale)
def test_verif_5(teardown):
    try:
        funcs.verif_h1("h1", "Wedding Dresses on Sale")
    except:
        assert False


print("'Wedding Dresses on Sale' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("Size Guide")


# Verifies "Measurement Guide" page (Home -> All wedding dresses -> Size Guide)
def test_verif_6(teardown):
    try:
        funcs.verif_h1("h1", "Measurement Guide")
    except:
        assert False


print("'Measurement Guide' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("Alteration Shops")


# Verifies "Alteration recommendations" page (Home -> All wedding dresses -> Alteration Shops)
def test_verif_7(teardown):
    try:
        funcs.verif_h1("h1", "Alteration recommendations")
    except:
        assert False


print("'Alteration recommendations' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("Our Brides")


# Verifies "Our Brides" page (Home -> All wedding dresses -> Our Brides)
def test_verif_8(teardown):
    try:
        funcs.verif_h1("h1", "Our Brides")
    except:
        assert False


print("'Our Brides' page confirmed!")


funcs.feb_cn("mega-menu-link")
funcs.feb_lt("Ready to Ship")


# Verifies "Ready To Ship" ("In Stock") page (Home -> All wedding dresses -> Ready to ship)
def test_verif_9(teardown):
    try:
        funcs.verif_in_stock("woof_remove_ppi", "h1")
    except:
        assert False


print("'Ready To Ship' page confirmed!")

