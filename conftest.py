# content of conftest.py
import pytest
from start_point import browser
@pytest.fixture(scope="module")
def teardown():
  yield browser
  browser.close()


def pytest_addoption(parser):
    parser.addoption(
        "--mode", action="store", default="gui", help="Type in browser mode headless/gui"
    )


@pytest.fixture(scope="module")
def mode(request):
    return request.config.getoption("--headless")
