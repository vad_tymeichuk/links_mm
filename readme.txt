1. In order to run tests type following command: pytest -s test_ui.py

2. When prompted "Headless mode? y/n":
 - type "y" and press Enter to run tests in "headless" mode
 - type "n" and press Enter to run tests in "gui" mode
